/** @jsx jsx */
import React from 'react'
import {Box, Flex, Text,Link, Button} from "rebass";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserAstronaut} from "@fortawesome/free-solid-svg-icons";
import {css, jsx} from "@emotion/core";
import Loading from "../shared/loading/loading";

interface IEmployeesList {
  deleteEmployee?: any,
  employees?:any
}
export default function EmployeesList({employees}:IEmployeesList){


  return(
	  <div>
		{employees !== undefined && employees.length === 0 ? <Loading /> : (
			<Box marginTop={50}>
			  <Flex flexWrap='wrap'  padding={'0 20px'} justifyContent={'center'}>
				{
				  employees.map((e)=>
					  <Box css={cardContainer}  margin={'5px'} width={1/4}  variant='card'>
						<Box color={'white'} textAlign={'center'} css={cardContnet}  p={3}
							 width={1}>
						  <FontAwesomeIcon size={'10x'} icon={faUserAstronaut}/>
						</Box>
						<Box color={'primary'} css={cardContnet}  p={3}
							 width={1}>
						  <Text
							  fontSize={[ 3, 4 ]}
							  fontWeight='bold'
							  fontFamily={'pt-sans, sans-serif'}
							  color='white'>
							{e.employee_name}
						  </Text>

						  <Text
							  fontSize={3}
							  fontWeight='light'
							  marginTop={2}
							  fontFamily={'pt-sans, sans-serif'}
							  color='white'>
							{e.employee_age} years old
						  </Text>

						  <Text
							  fontSize={[ 3, 4 ]}
							  marginTop={2}
							  fontFamily={'pt-sans, sans-serif'}
							  color='grey'>
							<span>$</span>
							{e.employee_salary}
						  </Text>
						</Box>
						<Box  css={cardContnet}>
						  <Button color={'white'} variant='outline' mr={2}>
							<Link variant='nav' href={`/employee/${e.id}`}>View</Link>
						  </Button>
						</Box>
					  </Box>
				  )
				}
			  </Flex>
			</Box>
		)}
	  </div>
  )
}

const cardContainer =
	css`
   display: flex;
   flex-direction: column;
   justify-content: center;
   background-image: url("https://i.redd.it/b3esnz5ra34y.jpg");
   background-size: cover;
   background-position: center, center;
    border-radius: 40px;
   box-shadow: 5px 5px 30px 7px rgba(0,0,0,0.1), -5px -5px 30px 7px rgba(0,0,0,0.1);
    cursor: pointer;
  transition: 0.4s;
   &:hover{
    transform: scale(0.9, 0.9);
  box-shadow: 5px 5px 30px 15px rgba(0,0,0,0.1), 
    -5px -5px 30px 15px rgba(0,0,0,0.1);
   }
`;


const cardContnet =
	css`
   align-self: center;
`;

