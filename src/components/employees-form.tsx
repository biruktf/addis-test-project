/** @jsx jsx */
import React,{useState} from 'react'
import {Box,Button } from "rebass";
import {
  Label,
  Input
} from '@rebass/forms'
import { css, jsx } from "@emotion/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserPlus} from "@fortawesome/free-solid-svg-icons";


const formContainer = css`
   width: 40%;
   padding-bottom: 20px !important;
   border-radius: 40px;
   position: absolute;
   top: 50%;
   left: 50%;
   transform: translate(-50%,-50%);
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-content: center;
   box-shadow: 0 5px 10px rgba(0,0,0,0.1);   
`;

const inputContainer = css`
   width: 50%;
   align-self: center;
   margin: 5px auto ;
   text-align: center;

`;

const formAvatar  =
	css`
   background-image: url("https://i.redd.it/b3esnz5ra34y.jpg");
   background-size: cover;
   background-position: center, center;
   transition: 0.4s;
   margin-bottom: 20px;
   height: 100px;
   display: flex;
   justify-content: center;
   border-top-left-radius: 40px;
   border-top-right-radius: 40px;
   box-shadow: 0 1px 10px rgba(0,0,0,0.1);
`;

const form =
	css`
   align-self: center;
`;

function EmployeesForm(props) {
  const { employee,saveEmployee } = props;
  const [name, setName] =  useState(employee? employee.employee_name:'');
  const [salary, setSalary] =  useState(employee? employee.employee_salary:'');
  const [age, setAge] =  useState(employee? employee.employee_age:'');
  const [id, setId] =  useState(employee? employee.id:null);


  const handleSubmit = (e) => {
	e.preventDefault();
	saveEmployee({id,name,salary,age})
  };

  return  (
	  <div>
		<form onSubmit={handleSubmit}>
		  <div
			  css={formContainer}
		  >
			<Box width={1} css={formAvatar}>
			  <Box color={'white'} css={form}>
				<FontAwesomeIcon size={"5x"} icon={faUserPlus}/>
			  </Box>
			</Box>


			<div css={inputContainer} >
			  <Label htmlFor='name'>Name</Label>
			  <Input
				  placeholder={'Joe doe'}
				  value={name}
				  id='name'
				  name='name'
				  onChange={e=>setName(e.target.value)}
			  />
			</div>
			<div css={inputContainer}>
			  <Label htmlFor='salary'>Salary</Label>
			  <Input
				  placeholder={'$2000'}
				  value={salary}
				  id='salary'
				  name='salary'
				  onChange={e=>setSalary(e.target.value)}
			  />
			</div>
			<div css={inputContainer}>
			  <Label htmlFor='age'>Age</Label>
			  <Input
				  placeholder={'23'}
				  value={age}
				  id='age'
				  name='age'
				  onChange={e=>setAge(e.target.value)}
			  />
			</div>


			{/*BUtton*/}
			<div css={inputContainer}>
			  <Button type={'submit'} marginBottom={3} width={1} marginTop={2}>
				Save
			  </Button>
			</div>
		  </div>
		</form>

	  </div>
  )
}


export default EmployeesForm


