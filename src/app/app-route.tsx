import * as React from 'react'
import { Route, Switch} from 'react-router-dom'
import EmployeeDetail from "../pages/employee-detail-page";
import EmployeeFormPage from "../pages/employee-add-page";
import Employees from "../pages/employees-page";
import PageNotFound from "../shared/page-not-found/page-not-found";

function AppRoute() {
  return(
	  <Switch>
		<Route exact path={'/employees'} component={Employees}/>
		<Route exact path={'/employees/new'} component={EmployeeFormPage}/>
		<Route exact path={'/employee/edit/:_id'} component={EmployeeFormPage}/>
		<Route exact path={'/employee/:_id'} component={EmployeeDetail}/>
		<Route exact path={'/'} component={Employees}/>
		<Route component={PageNotFound}/>
	  </Switch>
  )
}

export default AppRoute