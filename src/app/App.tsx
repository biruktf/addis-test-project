import * as React from 'react'
import Header from '../shared/header/header'
import Footer from '../shared/footer/footer'
import AppRoute from "./app-route";
import {BrowserRouter} from "react-router-dom";
import { ThemeProvider } from 'emotion-theming'
import theme from '@rebass/preset'
import {createStore, applyMiddleware} from "redux";
import rootReducer from '../stores/reducers/rootReducer'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { composeWithDevTools} from "redux-devtools-extension";

const store = createStore(
	rootReducer,
	composeWithDevTools(applyMiddleware(thunk))
)
function App() {
  return(
  	<BrowserRouter>
	 <Provider store = {store}>
	   <ThemeProvider theme={theme}>
		 <div>
		   <Header/>
		   <div
			   style={{minHeight:'80vh' }} >
			 <AppRoute/>
		   </div>
		   <Footer/>
		 </div>
	   </ThemeProvider>
	 </Provider>
	</BrowserRouter>
  )
}

export default App