import React, {useEffect, useState} from 'react'

import {connect} from "react-redux";
import { createEmployee, fetchEmployee, updateEmployee } from '../stores/actions/actions'
import {Redirect} from "react-router-dom";
import EmployeesForm from "../components/employees-form";
import {State} from "../stores/reducers/employees";
import Loading from "../shared/loading/loading";

//since this is a basic app didnt want to specify types
interface IEmployeeProps {
  updateEmployee?:any
  createEmployee?:any
  fetchEmployee?:any
  state?:any
  match?:any
}
function EmployeeFormPage(props:IEmployeeProps) {
  const  { state,match } = props

  const [isDone, setisDone] = useState(false);
  const [loading, setLoading]= useState(!!match.params._id);

  useEffect(()=>{
	if(match.params._id){
	  props.fetchEmployee(match.params._id)
		  .then(()=>{
		    setLoading(false)
		  })
		  .catch(e=>{
		    console.log(e.message)
		  })
	}
  },[]);


  const saveEmployee = ({id, name, salary, age}) => {
	if(id){
	  return props.updateEmployee({name, salary,age, id})
		  .then(()=> setisDone(true))
	}else {
	  return props.createEmployee({name, salary,age})
		  .then(()=>{setisDone(true)})
	}
  }

  return isDone?<Redirect to={'/employees'}/>: loading?(<Loading/>):(
	  <EmployeesForm
		  employee={state.employee}
		  saveEmployee={saveEmployee}
	  />
  )
}

const mapDispatchToProps = (dispatch)=> {
  return {
	createEmployee: (employee)=>dispatch(createEmployee(employee)),
	fetchEmployee: (id)=>dispatch(fetchEmployee(id)),
	updateEmployee: (employee)=>dispatch(updateEmployee(employee))
  }
};


function mapStateToProps(state:State) {
  return {
	state: state.employees
  }
}

export default connect(mapStateToProps,mapDispatchToProps )(EmployeeFormPage)