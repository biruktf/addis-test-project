import React,{ useEffect } from 'react'
import { connect } from 'react-redux'
import EmployeesList from '../components/employees-list'
import {fetchEmployees} from '../stores/actions/actions'


function Employees(props){

  const {employees} = props;

  useEffect(()=>{
	props.fetchEmployees();

  },[]);
  return(
	  <div>
		<EmployeesList employees ={employees.employees}/>
	  </div>
  )
}

function mapStateToProps(state) {
  return {
	employees: state.employees
  }
}

const mapDispatchToProps = (dispatch)=> {
  return {
	fetchEmployees: ()=>dispatch(fetchEmployees())
  }
};


export default connect(mapStateToProps,mapDispatchToProps)(Employees)