/** @jsx jsx */
import React, {useEffect, useState} from 'react'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faUserAstronaut } from "@fortawesome/free-solid-svg-icons";
import { Redirect} from "react-router-dom";
import {Box, Link,Button, Text} from "rebass";
import {connect} from "react-redux";
import {deleteEmployee, fetchEmployee} from "../stores/actions/actions";
import {css, jsx} from "@emotion/core";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons/faTrashAlt";
import {State} from "../stores/reducers/employees";
import Loading from "../shared/loading/loading";

const cardContainer =
	css`
   display: flex;
   width: 80%;
   margin: 0 auto;
   flex-direction: column;
   justify-content: center;
   background-image: url("https://i.redd.it/b3esnz5ra34y.jpg");
   background-size: cover;
   background-position: center, center;
   border-radius: 40px;
   position: absolute;
   top:50%;
   left: 50%;
   transform: translate(-50%,-50%);
   box-shadow: 5px 5px 30px 7px rgba(0,0,0,0.1), -5px -5px 30px 7px rgba(0,0,0,0.1);
  transition: 0.4s;
   &:hover{
   }
`;

const cardContnet =
	css`
   align-self: center;
   text-align: center;
`;

function EmployeeDetail(props) {

  const { match, state } = props;
  const [loading, setLoading]= useState(!!match.params._id);
  const [isDone, setisDone] = useState(false);

  useEffect(()=>{
	if(match.params._id){
	  props.fetchEmployee(match.params._id)
		  .then(()=>{
			setLoading(false)
		  })
		  .catch(e=>{
			console.log(e.message)
		  })
	}
  },[]);

  function handleDelete(){
    if(confirm(`Are You Sure You want to delete "${state.employee!.employee_name}"`)){
      setisDone(true);
	  props.deleteEmployee(props.state.employee.id)
	}else {
      alert('Employee Not Deleted');
	}
  }
  return isDone?<Redirect to={'/employees'}/> : loading? <Loading/>: (
  	<div>
	  <Box  css={cardContainer}  margin={'5px'} width={1/4}  variant='card'>
		<Box color={'white'} css={cardContnet}  p={3}
			 width={1}>
		  <FontAwesomeIcon size={'10x'} icon={faUserAstronaut}/>
		</Box>

		<Box color={'primary'} css={cardContnet}  p={3}
			 width={1}>
		  <Text
			  fontSize={[ 3, 4 ]}
			  fontWeight='bold'
			  fontFamily={'pt-sans, sans-serif'}
			  color='white'>
			{state.employee?state.employee.employee_name:''}
		  </Text>
		  <Text
			  fontSize={3}
			  fontWeight='light'
			  marginTop={2}
			  fontFamily={'pt-sans, sans-serif'}
			  color='white'>
			{state.employee?state.employee.employee_age:''} years old
		  </Text>
		  <Text
			  fontSize={[ 3, 4 ]}
			  marginTop={2}
			  fontFamily={'pt-sans, sans-serif'}
			  color='grey'>
			<span>$</span>
			{state.employee?state.employee.employee_salary:''}
		  </Text>
		</Box>
		<Box css={cardContnet}>
		 <Link variant={'nav-link'} href={`/employee/edit/${state.employee?state.employee.id:''}`}>
		   <Button cursor={'pointer'} color={'white'} variant='outline' mr={2}>
			 <FontAwesomeIcon icon={faEdit}/>
		   </Button>
		 </Link>
		  <Button onClick={handleDelete} cursor={'pointer'} color={'white'} variant='outline' mr={2}>
			<FontAwesomeIcon icon={faTrashAlt}/>
		  </Button>
		</Box>
	  </Box>
	</div>
  )
}

const mapDispatchToProps = (dispatch)=> {
  return {
	fetchEmployee: (id)=>dispatch(fetchEmployee(id)),
	deleteEmployee: (id)=>dispatch(deleteEmployee(id))
  }
};

function mapStateToProps(state:State) {
  console.log('Detail Map to Props,\n', state);
  return {
	state: state.employees
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(EmployeeDetail)