import * as React from 'react'
import * as ReactDOM from 'react-dom'
import App from "./app/App";
import './assets/styles/index.scss'

const ROOT = document.getElementById('root');

ReactDOM.render(<App/>, ROOT);