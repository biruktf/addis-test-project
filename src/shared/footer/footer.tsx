import * as React from 'react'
import styled from '@emotion/styled'


const MyFooter = styled.div`
  padding: 32px;
  text-align: center;
  background-image: linear-gradient(
    to bottom,
    transparentize($fg, 0.95) 0%,
    transparent 70%
  );
  &:hover {
    color: dodgerblue;
  }
`
function Footer() {
  return <MyFooter>&copy; 2019 Addis sw</MyFooter>
}

export default Footer