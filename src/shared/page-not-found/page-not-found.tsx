import * as React from 'react'
/** @jsx jsx */
import {css, jsx } from "@emotion/core";
import { Box, Text} from 'rebass'
import {Button, Link} from 'rebass'
const cardContainer =
	css`
   width: 80%;
   height: 200px;
   margin: 30px auto;
   display: flex;
   flex-direction: column;
   justify-content: space-evenly;
`;
const btn =
	css`
	  align-self: center;
	  transition: .3s;
	  &:hover{
	   transform: scale(0.9);
	  }
	`;

function PageNotFound() {
  return(
	  <Box   css={cardContainer}>
		  <Text color={'black'} alignSelf={'center'} fontSize={[4,5,6]}>
			404! 😕 Page Not Found
		  </Text>
		<Button css={btn} width={1/5} variant={'primary'}>
		  <Link variant={'nav'} href={'/employees'}>
			<Text color={'white'} fontSize={2}>
			  Go back to Home
			</Text>
		  </Link>
		</Button>
	  </Box>
  )
}

export default PageNotFound