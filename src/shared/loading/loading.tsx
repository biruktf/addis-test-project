/** @jsx jsx */
import React from 'react';
import ReactLoading from 'react-loading';
import { css, jsx } from "@emotion/core";
import styled from '@emotion/styled'

const LoadingContainer = styled.div`
      position: absolute;
      top: 50%;
      left: 45%;
      text-align: center;
`;
const Loading = () => (
	<LoadingContainer>
	  <ReactLoading type={'bubbles'} color={'dodgerblue'} height={100} width={100} />
	</LoadingContainer>
);

export default Loading;