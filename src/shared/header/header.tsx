import * as React from 'react'
/** @jsx jsx */
import { css, jsx} from '@emotion/core'
import styled from '@emotion/styled'
import {
  Box,
  Flex,
  Text,
	Link
} from 'rebass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserPlus} from "@fortawesome/free-solid-svg-icons";

const headerContainer = css`
      padding: 0 10px !important;
  `;

const Span = styled.span`
  font-size: 24px;
  margin-left: 4px;
  color: white;
  font-weight: lighter;
`;
const icon = css`
     padding: 15px;
     display:flex;
      &:hover {
        color:white;
        text-decoration:none;
      }
  `;
function Header() {

  return(
		  <Flex
			  css={headerContainer}
			  px={2}
			  py={3}
			  color='lightgray'
			  bg='primary'
			  alignItems='center'>
			<Link
				css={icon}
				variant={'nav'}
				href='/employees'
			>
			  <Text p={2} color={'white'}  fontWeight='bold' fontSize={[2,3,4]}>Employees</Text>
			</Link>
			<Box mx='auto' />
			<Link
				css={icon}
				variant={'nav'}
				href='/employees/new'
			>
			  <Span>
				<FontAwesomeIcon icon={faUserPlus}/>
			  </Span>
			</Link>
		  </Flex>
  )
}

export default Header