import axios from 'axios/index'
import {EMPLOYEE_FETCHED, SET_EMPLOYEES, EMPLOYEE_DELETED, EMPLOYEE_UPDATED, SAVE_EMPLOYEE} from "./actionTypes";

const apiURL = '\thttp://dummy.restapiexample.com/api/v1';



export function setEmployee(employee) {
    return {
        type: EMPLOYEE_FETCHED,
        employee
    }
}
export function fetchEmployee(id) {
    return dispatch => {
        return  axios
            .get(`${apiURL}/employee/${id}`)
            .then(res => res.data)
            .then(employee => dispatch(setEmployee(employee)))
            .catch(e=>console.log(e))
    }
}

export function setEmployees(employees) {
    return {
        type: SET_EMPLOYEES,
        employees
    }
}
export function fetchEmployees(){
    return dispatch => {
      return  axios
            .get(`${apiURL}/employees`)
            .then(res => res.data)
            .then(employees => {
            console.log('EMPLOYEES => ',employees);
            dispatch(setEmployees(employees))
            })
            .catch(e=>console.log(e))
    }
}

export function saveEmployee(employee) {
    return {
        type: SAVE_EMPLOYEE,
        employee
    }
}
export function createEmployee({ name, salary, age}) {
    return (dispatch) => {
        return axios.post(`${apiURL}/create`, {name, salary, age})
            .then(res => res.data)
            .then(employee => dispatch(saveEmployee(employee)))
            .catch(error => {
                throw(error);
            });
    };
}

export function employeeUpdated(employee) {
    return {
        type: EMPLOYEE_UPDATED,
        employee
    }
}
export function updateEmployee({ name, salary, age, id}) {
    return (dispatch) => {
        return axios.put(`${apiURL}/update/${id}`, {name, salary, age})
            .then(res => res.data)
            .then(employee => dispatch(employeeUpdated(employee)))
            .catch(error => {
                throw(error);
            });
    };
}

export function employeeDeleted(id) {
    return {
        type: EMPLOYEE_DELETED,
        id
    }
}
export function deleteEmployee(id) {
    return (dispatch) => {
        return axios.delete(`${apiURL}/delete/${id}`)
            .then(res => dispatch(employeeDeleted(id)))
            .catch(error => {
                console.log('ERROR FROM ==', error.message);
                throw(error);
            });
    };
}

