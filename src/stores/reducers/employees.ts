import {SAVE_EMPLOYEE, SET_EMPLOYEES, EMPLOYEE_FETCHED, EMPLOYEE_UPDATED, EMPLOYEE_DELETED} from "../actions/actionTypes";

export type IEmployee = {
    employee_name: string,
    employee_age: string,
    employee_salary: string,
    id: string
}
export type Action =
    | { type: 'SAVE_EMPLOYEE'; employee: IEmployee }
    | { type: 'SET_EMPLOYEES'; employees: IEmployee[] }
    | { type: 'EMPLOYEE_FETCHED'; employee: IEmployee }
    | { type: 'EMPLOYEE_UPDATED'; employee: IEmployee }
    | { type: 'EMPLOYEE_DELETED'; id: string }

export const initialState: State = {
    employees: [],
};

export type State = {
    employees: IEmployee[],
    employee?:IEmployee
}
export default function employees(state = initialState, action:Action) {
    switch (action.type) {
        case SET_EMPLOYEES:
            return {
                ...state,
                employees: action.employees
            };
        case EMPLOYEE_UPDATED:
            return {
                ...state,
                employee: state.employees.map((item:IEmployee) => {
                    if(item.id === action.employee.id) return {...state,employee: action.employee};
                    return {...state, employee: item}
                })
            };
        case EMPLOYEE_DELETED:
            return {...state, employees: state.employees.filter((item:IEmployee) => item.id !== action.id)};
        case EMPLOYEE_FETCHED:
                return {
                    ...state,
                    employee:action.employee
                };
        case SAVE_EMPLOYEE:
            return {
                ...state,
                employee: action.employee
            };
        default:
            return state
    }
}