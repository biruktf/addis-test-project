
## Addis Test Project

This project is created for a test project, using 
* babel 
* webpack
* react and react-dom
* redux
* emotion 
* rebass
* and using [dummy restApi](http://dummy.restapiexample.com/) for testing the components and logic

### typescript
the project comes with typescript support, if you are not familiar with
typescript make sure to visit [Typescript](https://www.typescriptlang.org/)


### `instructions`
1. clone the project
2. cd into the dir
3. run `yarn install` or if you are using npm `npm install`
4. `yarn start` or `npm start`






